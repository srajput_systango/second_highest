
require_relative 'input_reader'
require_relative 'compare'

read_data = InputReader.new("input.txt")
check = read_data.check_file
data_array = read_data.data_extract_file

compare = Compare.new(data_array)
second_high = compare.second_highest

puts second_high