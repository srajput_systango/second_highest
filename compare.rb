
class Compare

	def initialize(array)
		@array = array
	end

	def second_highest

		@max_value = @array[0]
		@second_value = @array[1]
		@multiple = @max_value * @second_value

		for i in 2..(@array.length-1)

			@temporary = @max_value*@array[i]

			if @multiple <= @temporary
				if @max_value <= @array[i] 
					@second_value = @max_value
					@max_value = @array[i]
				else
					if @second_value <= @array[i]
						@second_value = @array[i]
				end
				end
			end
		
		end

		return @second_value
	end
end

