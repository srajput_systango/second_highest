class InputReader
	
	def initialize(file_name)
		@file_name=file_name
	end

	def check_file
		if File.exists?(@file_name)
			return true
		else
			puts "File doesn't exists,Program is Terminating"
			exit
		end
	end

	def data_extract_file
		
		@data = Array.new
		File.open("input.txt", "r") do |read|
  			read.each_line do |line|
   		 	@data<<line.to_i
 		 	end
		end
		return @data
	end
end